-- Setup script for local testing
CREATE USER amivburger IDENTIFIED BY 'amivburgerpassword';
CREATE DATABASE amivburger;
GRANT ALL PRIVILEGES ON amivburger.* TO amivburger;
