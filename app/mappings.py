'''
Contains dictionaries mapping IDs from the burger creator form
to the corresponding ingredients
'''
BREAD = {
1: 'Sesam Bun',
2: 'Brioche Bun',
3: 'Brezel Bun',
4: 'Farmer Bun',
5: 'Red Bun',
6: 'White Bun',
7: 'Black Bun',
8: 'Yellow Bun',
}

PATTY = {
1: 'Beef Burger',
2: 'Pulled Pork',
3: 'Poulet Burger',
4: 'Chicken Katsu',
5: 'Vegane Burger',
6: 'Vegetable Burger',
7: 'Quinoa Chili Burger',
8: 'Falafel Burger',
}

TOPPINGS = {
1: 'Lettuce',
2: 'Cole Slaw',
3: 'Cheddar',
4: 'Feta',
5: 'Onions',
6: 'Roasted Onions',
7: 'Avocado',
8: 'Jalapenos',
9: 'Olives',
10: 'Tomato',
11: 'Pickle',
12: 'Rucola',
13: 'Guacamole',
14: 'Egg',
15: 'Bacon',
16: 'Chorizo',
17: 'Grilled Vegetables',
18: 'Honey-Mustard Sauce',
19: 'Hot-Ketchup',
20: 'Truffle Mayonnaise',
21: 'Cocktail Sauce',
22: 'Tartare Sauce',
23: 'BBQ Sauce',
24: 'Pesto',
}

SIDE = {
1: 'Pommes Frites',
2: 'Country Cuts',
3: 'Chips',
4: 'Onion Rings',
}
