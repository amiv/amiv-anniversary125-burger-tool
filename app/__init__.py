from flask import Flask, g, session
from flask_wtf.csrf import CSRFProtect
from .models import db
from nethz.ldap import AuthenticatedLdap

# Set up Flask app and load configuration
app = Flask(__name__)
app.config.from_pyfile('./config.py')
app.secret_key = app.config['SECRET_KEY']

# Initialize LDAP auth
ldap_connector = AuthenticatedLdap(app.config['LDAP_USERNAME'],
                                     app.config['LDAP_PASSWORD'])

# Initialize CSRFProtect extension
csrf = CSRFProtect(app)

# Bind SQLAlchemy to Flask app
db.init_app(app)

from app import views
