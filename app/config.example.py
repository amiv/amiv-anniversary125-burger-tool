'''
Essential app configuration
'''

SECRET_KEY = 'THIS_SHOULD_BE_RANDOM_AND_SECRET'

MYSQL_USERNAME = 'amivburger'
MYSQL_PWD = 'amivburgerpassword'
MYSQL_HOST = 'localhost'
MYSQL_DB = 'amivburger'

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(MYSQL_USERNAME, MYSQL_PWD, MYSQL_HOST, MYSQL_DB)
# Suppress deprecation warning
SQLALCHEMY_TRACK_MODIFICATIONS = False

LDAP_USERNAME = 'amiv_ldap_username'
LDAP_PASSWORD = 'amiv_ldap_password'
