$(document).ready(function() {
  $("select.image-picker").imagepicker({
    hide_select:  false,
  });
  
  $("select.image-picker.show-labels").imagepicker({
    hide_select:  false,
    show_label:   true,
  });
  
  $("select.image-picker.limit_callback").imagepicker({
    limit_reached:  function(){alert('We are full!')},
    hide_select:    false
  });
  
  var container = $("select.image-picker.masonry").next("ul.thumbnails");
  container.imagesLoaded(function(){
    container.masonry({
      itemSelector:   "li",
    });
  });

  $("form").submit(function(event) {
    // TODO: ...
  });
});

// center modals in viewport
var messages = document.getElementsByClassName('message');
for(var i = 0; i < messages.length; i++) {
    messages[i].style.left = (window.innerWidth - messages[i].offsetWidth) / 2 + 'px';
}

window.setTimeout(function() {
  for(var i = 0; i < messages.length; i++) {
    messages[i].style.display = 'none';
  }
}, 2000);


// add event listener for facebook share button
document.getElementById('fbShareButton').addEventListener('click', function () {
  FB.ui({
      method: 'share',
      href: 'https://developers.facebook.com/docs/',
  }, function (response) { });
});
