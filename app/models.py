'''
Contains model definitions for SQLAlchemy ORM
'''
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Burger(db.Model):
    '''
    Represents a Burger created by a user.
    
    Each user can create at most one burger.
    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=128), unique=True, nullable=False)
    description = db.Column(db.Text, unique=False, nullable=False)
    nethz = db.Column(db.String(64), unique=True, nullable=False)
    timestamp = db.Column(db.DateTime, unique=False, nullable=False)

    # Burger components
    bread = db.Column(db.Integer, unique=False, nullable=False)
    patty = db.Column(db.Integer, unique=False, nullable=False)
    toppings = db.Column(db.String(64), unique=False, nullable=False)
    side = db.Column(db.Integer, unique=False, nullable=False)


class Vote(db.Model):
    '''
    Represents a single vote by a single user.

    One user can vote as many times as he / she wishes. However, a user can at most give one vote
    to a given burger.
    '''
    id = db.Column(db.Integer, primary_key=True)
    nethz = db.Column(db.String(64), unique=False, nullable=False)
    burger_id = db.Column(db.Integer, db.ForeignKey('burger.id'), nullable=False)
    burger = db.relationship('Burger', backref=db.backref('votes', lazy=True))
