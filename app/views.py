'''
Contains all views
'''
from app import app, ldap_connector
from datetime import datetime
from flask import flash, g, redirect, render_template, request, session, url_for
from sqlalchemy.sql import text
from .mappings import BREAD, PATTY, TOPPINGS, SIDE
from .models import db, Burger, Vote


PAST_DEADLINE = datetime.now() > datetime(2018, 3, 28, 23, 59, 59)


@app.route('/')
def index():
  '''
  Front page. Shows a list of all burgers 
  '''
  # Sort
  sort = request.args.get('sort')
  if sort == 'top' or PAST_DEADLINE:
    order_by = 'vote_count DESC, timestamp'
  else:
    order_by = 'timestamp DESC'

  # Fetch list of burgers with corresponding vote info
  # Could probably also be done with SQLAlchemy but too lazy to look up how
  querystring = text('SELECT burger.id AS id, name, description, burger.nethz AS nethz, timestamp, '
                  'bread, patty, toppings, side, COUNT(vote_1.id) as vote_count, vote_2.id as vote_id '
                  'FROM burger '
                  'LEFT JOIN vote AS vote_1 ON vote_1.burger_id=burger.id '
                  'LEFT JOIN vote AS vote_2 ON vote_2.burger_id=burger.id AND vote_2.nethz= :nethz ' 
                  'GROUP BY burger.id '
                  'ORDER BY {}'.format(order_by))
  
  # Display, which burgers have already been liked
  if 'user' in session:
    burger_list = db.engine.execute(querystring, nethz=session['user']).fetchall()
  else:
    burger_list = db.engine.execute(querystring, nethz='').fetchall()

  if PAST_DEADLINE:
    return render_template('deadline.html',
                          winner=burger_list[0],
                          bread=BREAD,
                          patty=PATTY,
                          toppings=TOPPINGS,
                          side=SIDE)

  return render_template('vote.html',
                          burger_list=burger_list,
                          bread=BREAD,
                          patty=PATTY,
                          toppings=TOPPINGS,
                          side=SIDE,
                          sort=sort)


@app.route('/vote', methods=['POST'])
def vote():
  '''
  Handles POST requests for voting
  '''
  def die(message, burger_id=None):
    '''
    Display an error-level flash message and return
    '''
    flash(message, 'error')
    # Redirect without anchor
    if not burger_id:
      redirect(url_for('index'))
    # Redirect with anchor
    return redirect('{}/#burger-{}'.format(url_for('index'), burger_id))

  # Check if we're within the voting period
  if PAST_DEADLINE:
    return die('The winning burger has already been chosen. Voting is no longer possible.')

  # Check if the user is logged in
  if not 'user' in session:
    return redirect('{}?next=/'.format(url_for('login')))
  
  # Check if the burger_id is valid
  burger_id = request.form.get('burger_id')
  if not burger_id or not Burger.query.get(burger_id):
    return die('Invalid burger ID')

  # User is casting their vote
  if request.form.get('action') == 'like':
    # Check if the user hasn't already voted for the same burger
    if Vote.query.filter_by(nethz=session['user'], burger_id=burger_id).first():
      return die('You have already liked this burger.', burger_id)
    
    # Add vote
    vote = Vote(
      nethz=session['user'],
      burger_id=burger_id,
    )
    db.session.add(vote)
    db.session.commit()
    flash('Thanks for voting! You can vote for multiple burgers!', 'success')

  # User is revoking their vote
  elif request.form.get('action') == 'unlike':
    # Find the vote
    vote = Vote.query.filter_by(nethz=session['user'], burger_id=burger_id).first()

    if vote:
      # Delete the vote
      db.session.delete(vote)
      db.session.commit()

    flash('Your like has been removed.', 'success')

  return redirect('{}/#burger-{}'.format(url_for('index'), burger_id))


@app.route('/login', methods=['GET', 'POST'])
def login():
  '''
  Login page. LDAP-Login only.
  '''
  # Already logged in or past voting deadline, redirect to front page
  if 'user' in session or PAST_DEADLINE:
      return redirect(url_for('index'))

  if request.method == 'POST':
    # Login in debug mode for testing
    if 'DEBUG' in app.config and app.config['DEBUG']:
      session['user'] = app.config['LDAP_TEST_USER']
      next_url = request.form['next'] if request.form['next'] else '/'
      return redirect(next_url)

    user = request.form['user']
    passwd = request.form['passwd']
    # Attempt to log in using LDAP
    if ldap_connector.authenticate(user, passwd):
      session['user'] = user
      # Redirect the user to the path specified in 'next', otherwise to the front page
      next_url = request.form['next'] if request.form['next'] else '/'
      return redirect(next_url)
    else:
      flash('Invalid credentials', 'error')
        
  return render_template('login.html')


@app.route('/logout')
def logout():
  '''
  Logout page
  '''
  session.pop('user', None)
  flash('Successfully logged out.')
  return redirect(url_for('index'))


@app.route('/new', methods=['GET', 'POST'])
def create():
  '''
  Burger creator
  '''
  def die(message):
    '''
    Display an error-level flash message and return
    '''
    flash(message, 'error')
    return render_template('creator.html')
  
  if not 'user' in session:
    return redirect('{}?next=/new'.format(url_for('login')))

  if PAST_DEADLINE:
    return die('The winning burger has already been chosen. Submitting new burgers is no longer possible.')

  # A new burger was submitted
  if request.method == 'POST':
  
    # Perform field validation
    bread = request.form.get('bread')
    if not bread:
      return die('Please select a bread for your burger.')
    
    patty = request.form.get('patty')
    if not patty:
      return die('Please select a patty for your burger.')
  
    toppings = request.form.getlist('toppings')
    if not toppings:
      return die('Please select at least one topping for your burger.')
    if len(toppings) > 6:
      return die('Please select at most six toppings for your burger.')
    
    side = request.form.get('side')
    if not side:
      return die('Please select a side for your burger.')

    name = request.form.get('name')
    if not name:
      return die('Please give your burger a name.')

    description = request.form.get('description')
    if not description:
      return die('Please describe your burger in a few short sentences.')
    
    if len(description) > 200:
      return die('Please describe your burger in no more than 200 characters.')

    # Check that the user hasn't already created a burger
    if Burger.query.filter_by(nethz=session['user']).first():
      # In this case, send the user back to the voting page
      flash('You can only create one burger.', 'error')
      return redirect(url_for('index'))

    # All set, now save the burger in the database
    burger = Burger(
      name=name,
      description=description,
      nethz=session['user'],
      timestamp=datetime.now(),
      bread=bread,
      patty=patty,
      toppings=', '.join(toppings),
      side=side,
    )
    db.session.add(burger)
    db.session.commit()

    flash('Congrats on your new burger!', 'success')
    return redirect(url_for('index'))

  return render_template('creator.html')
