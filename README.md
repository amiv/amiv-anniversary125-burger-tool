# Burger-Tool (AMIV Anniversary 125 years)

A Webapp to select the most popular burger as AMIVs 125 year anniversary burger.

## Setup

### Virtualenv

The website runs using flask, and we recommend running it in a virtual
environment. To do this, you first need to initialise it:

```bash
\$ virtualenv venv
```

This will initialise the virtual environment in the `venv` directory. To
activate it, source the following script:

```bash
\$ source venv/bin/activate
```

If that was successful, your `$PS1` should start with `(venv)`.

### Dependencies

Now you need to install the dependencies listed in `requirements.txt`.

```bash
\$ pip install -r requirements.txt
```

### Database & Admin User

To create the database structure and add an admin user, run

```bash
\$ ./create_schema.py
```

## Run

After that, you can run the website by executing `amiv-anniversary125-burger-tool.py`. The website is
served at `127.0.0.1:5000` by default.
