'''
Entry point. Calls the app's run() method
'''
from app import app

app.run()
