'''
Helper script to recreate the database schema after changes have been made to models.py
'''
import app
# Fix for a weird bug which occurs when using db.init_app(app)
app.db.app = app.app

# Nuke current db schema
app.db.drop_all()

# Recreate schema
app.db.create_all()
